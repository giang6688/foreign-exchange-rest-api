package com.singtel.assignment.exception;

public class ExchangeRateNotFoundException extends Exception {

	private static final long serialVersionUID = 5484505340602284396L;

	public ExchangeRateNotFoundException(String message) {
		super(message);
	}
}

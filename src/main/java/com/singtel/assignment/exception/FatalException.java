package com.singtel.assignment.exception;

public class FatalException extends RuntimeException {

	private static final long serialVersionUID = -6704292146025227992L;
	
	public FatalException(String message) {
		super(message);
	}
}

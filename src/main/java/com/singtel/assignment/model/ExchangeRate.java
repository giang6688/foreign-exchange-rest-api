package com.singtel.assignment.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ExchangeRate {
	
	private String localCurrency;
	
	private Double rate;
	
	private String foreignCurrency;
	
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "en_GB", timezone="Asia/Singapore")
	private Date date;
	
	public ExchangeRate() {
	}

	public ExchangeRate(Date date, String localCurrency, Double rate, String foreignCurrency) {
		this.date = date;
		this.localCurrency = localCurrency;
		this.rate = rate;
		this.foreignCurrency = foreignCurrency;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getForeignCurrency() {
		return foreignCurrency;
	}

	public void setForeignCurrency(String foreignCurrency) {
		this.foreignCurrency = foreignCurrency;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "[" + date + "||" + localCurrency + "||" + rate + "||" + foreignCurrency + "]" ;
	}
}

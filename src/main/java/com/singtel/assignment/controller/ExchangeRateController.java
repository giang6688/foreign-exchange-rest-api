package com.singtel.assignment.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.singtel.assignment.config.RateFactory;
import com.singtel.assignment.exception.ExchangeRateNotFoundException;
import com.singtel.assignment.model.ExchangeRate;

@RestController
@RequestMapping("exchangeRates/")
public class ExchangeRateController {
	private static final Logger logger = LoggerFactory.getLogger(ExchangeRateController.class);
	
	@Autowired
	RateFactory rateFactory;
	
	@RequestMapping(method = RequestMethod.GET, value="/getAll/{date}")
	public List<ExchangeRate> getAll(
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws ExchangeRateNotFoundException {
		
		return rateFactory.getAllByDate(date);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/getRange/{currency}")
	public List<ExchangeRate> getRange(
			@PathVariable("currency") String currency, 
			@RequestParam(value = "from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date from,
			@RequestParam(value = "to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date to) throws ExchangeRateNotFoundException {
		
		return rateFactory.getAllInRange(currency, from, to);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/getRate")
	public ExchangeRate getRate(
			@RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
			@RequestParam(value = "lcy") String localCurrency,
			@RequestParam(value = "fcy") String foreignCurrency
			) throws ExchangeRateNotFoundException {
		
		return rateFactory.getExchangeRate(date, localCurrency, foreignCurrency);
	}
	
	/**
	 * Default exception handler
	 */
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="No Exchange rate found!")
	@ExceptionHandler({ ExchangeRateNotFoundException.class })
    public void handleException(ExchangeRateNotFoundException e) {
		logger.error(e.getMessage(), e);
	}
	
}

package com.singtel.assignment.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.singtel.assignment.exception.ExchangeRateNotFoundException;
import com.singtel.assignment.exception.FatalException;
import com.singtel.assignment.model.ExchangeRate;

/**
 * Service providing all exchange rates information. Exchange rate's information
 * will be loaded on application start up.
 * 
 * @author giang
 *
 */
@Service
public class RateFactory {
	private static final Logger logger = LoggerFactory.getLogger(RateFactory.class);

	private Map<Date, List<ExchangeRate>> rateByDate = new ConcurrentHashMap<>();

	private static final Pattern ratePattern = Pattern.compile("traded at\\s+(.*?)\\s+times");

	private static final String BASE_CURRENCY = "USD";

	@Value("${app.source.dir:data}")
	private String sourceFolder;

	/**
	 * On-startup function Looking for folder with file(s) which containing exchange rate information wrt to USD
	 * Reading & processing input files in parallel
	 */
	@PostConstruct
	public void init() {
		// Treat all files under data folder as an input
		File inputDir = new File(sourceFolder);
		if (inputDir.exists() && inputDir.isDirectory()) {
			int concurrentProcessors = Runtime.getRuntime().availableProcessors();
			if (inputDir.listFiles().length < concurrentProcessors) concurrentProcessors = inputDir.listFiles().length;

			ExecutorService executor = Executors.newFixedThreadPool(concurrentProcessors);
			for (File rateFile : inputDir.listFiles()) {
				executor.execute(new Runnable() {
					@Override
					public void run() {
						try (BufferedReader br = new BufferedReader(new FileReader(rateFile))) {
							Date processingDate = new SimpleDateFormat("yyyy-MM-dd").parse(rateFile.getName());
							List<ExchangeRate> rates = new ArrayList<>();

							String line;
							while ((line = br.readLine()) != null) {
								String currency = line.substring(2, 5);
								Matcher matcher = ratePattern.matcher(line);
								if (matcher.find()) {
									String rate = matcher.group(1);
									rates.add(new ExchangeRate(processingDate, currency, Double.valueOf(rate), BASE_CURRENCY));
								}
							}
							logger.debug("Adding {} exchange rates for position date {}", rates.size(), processingDate);
							rateByDate.put(processingDate, rates);
						} catch (IOException e) {
							logger.error(e.getMessage(), e);
						} catch (ParseException e) {
							logger.error("Failed to parse file name=" + rateFile.getName(), e);
						}
					}
				});
			}
			
			try {
				executor.shutdown();
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		} else {
			logger.error("Failed to find folder [{}]", inputDir);
			throw new FatalException("Failed to innitialize rate factory! Please check input files/folder.");
		}
	}

	/**
	 * Get all exchange rate(s) by given date
	 * 
	 * @param givenDate
	 * @return list of {@link ExchangeRate} with respect to USD
	 * @throws ExchangeRateNotFoundException if no exchange rate information was found
	 */
	public List<ExchangeRate> getAllByDate(Date givenDate) throws ExchangeRateNotFoundException {
		if (rateByDate.containsKey(givenDate)) {
			return rateByDate.get(givenDate);
		} else {
			throw new ExchangeRateNotFoundException("Exchange rate for [" + givenDate + "] not found!");
		}
	}

	/**
	 * get exchange rate for given date with given local & foreign currency also
	 * specified
	 * 
	 * @param givenDate
	 * @param localCurrency
	 * @param foreignCurrency
	 * @return {@link ExchangeRate}
	 * @throws ExchangeRateNotFoundException if no exchange rate information was found
	 */
	@Cacheable("getExchangeRate")
	public ExchangeRate getExchangeRate(Date givenDate, String localCurrency, String foreignCurrency) throws ExchangeRateNotFoundException {
		logger.debug("Find exchange rate for [{}::{}::{}]", givenDate, localCurrency, foreignCurrency);
		
		if (rateByDate.containsKey(givenDate)) {
			Optional<ExchangeRate> lcRate = rateByDate.get(givenDate).parallelStream()
					.filter(ex -> ex.getLocalCurrency().equalsIgnoreCase(localCurrency)).findFirst();
			
			if (foreignCurrency.equalsIgnoreCase(BASE_CURRENCY)) {
				if (lcRate.isPresent()) return lcRate.get();
			} else if (lcRate.isPresent()) {
				Optional<ExchangeRate> fcRate = rateByDate.get(givenDate).parallelStream()
						.filter(ex -> ex.getLocalCurrency().equalsIgnoreCase(foreignCurrency)).findFirst();
				if (fcRate.isPresent()) {
					Double calculatedRate = lcRate.get().getRate() / fcRate.get().getRate();
					return new ExchangeRate(givenDate, localCurrency, calculatedRate, foreignCurrency);
				}
			}
		}
		
		throw new ExchangeRateNotFoundException("Exchange rate for date [" + givenDate + "] "
				+ "with local-currency [" + localCurrency + "] and foreign-currency [" + foreignCurrency + "] "
						+ "not found!");
	}

	/**
	 * Get list of exchange rate(s) within a range specified by <b>from-date</b> &
	 * <b>to-date</b>
	 * 
	 * @param localCurrency
	 *            currency to find exchange rate
	 * @param from
	 *            start of date range
	 * @param to
	 *            end of date range
	 * @return list of {@link ExchangeRate} with respect to USD
	 * @throws ExchangeRateNotFoundException if no exchange rate information was found
	 */
	@Cacheable("getAllInRange")
	public List<ExchangeRate> getAllInRange(String localCurrency, Date from, Date to) throws ExchangeRateNotFoundException {
		List<ExchangeRate> possibleMatchings = rateByDate.entrySet().stream()
				.filter(e -> from.before(e.getKey()) || from.equals(e.getKey()))
				.filter(e -> to.after(e.getKey()) || to.equals(e.getKey())).flatMap(entry -> entry.getValue().stream())
				.collect(Collectors.toList());

		if (possibleMatchings != null && !possibleMatchings.isEmpty()) {
			return possibleMatchings.parallelStream().filter(e -> e.getLocalCurrency().equalsIgnoreCase(localCurrency))
					.collect(Collectors.toList());
		}

		throw new ExchangeRateNotFoundException("Exchange rate with given criteria not found! [currency:" + localCurrency + "] "
				+ "|| from:" + from + " "
				+ "|| to:" + to + "]");
	}
}

package com.singtel.assignment;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.singtel.assignment.config.RateFactory;
import com.singtel.assignment.exception.ExchangeRateNotFoundException;
import com.singtel.assignment.model.ExchangeRate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RateFactoryTest {

	@Autowired
	RateFactory rateFactory;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Test
	public void getAll_ByDate_Return7ExchangeRates() throws ExchangeRateNotFoundException, ParseException {
		List<ExchangeRate> rates = rateFactory.getAllByDate(sdf.parse("2017-01-01"));
		assertEquals(7, rates.size());
	}
	
	@Test(expected = ExchangeRateNotFoundException.class)
	public void getAll_ByDate_ExceptionThrown() throws ExchangeRateNotFoundException, ParseException {
		rateFactory.getAllByDate(sdf.parse("2019-01-01"));
	}
	
	@Test
	public void get_ExchangeRateBetweenSGDAndGBP() throws ExchangeRateNotFoundException, ParseException {
		ExchangeRate rate = rateFactory.getExchangeRate(sdf.parse("2017-01-01"), "SGD", "GBP");
		assertEquals(new Double(0.57), rate.getRate());
	}
	
	@Test
	public void getAll_ExchangeRatesInRange() throws ExchangeRateNotFoundException, ParseException {
		List<ExchangeRate> rates = rateFactory.getAllInRange("SGD", sdf.parse("2017-01-01"), sdf.parse("2017-02-15"));
		assertEquals(2, rates.size());
	}
	
	@Test(expected = ExchangeRateNotFoundException.class)
	public void get_ExchangeRateBetweenVNDAndGBP_ExceptionThrown() throws ExchangeRateNotFoundException, ParseException {
		rateFactory.getExchangeRate(sdf.parse("2017-01-01"), "VND", "GBP");
	}
}

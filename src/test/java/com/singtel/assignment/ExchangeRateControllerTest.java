package com.singtel.assignment;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@WebAppConfiguration
public class ExchangeRateControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getAll_ByDate() throws Exception {
		mockMvc.perform(get("/exchangeRates/getAll/2017-01-01")).andExpect(status().isOk());
	}

	@Test
	public void getAll_ByDateRangeAndCurrency() throws Exception {
		mockMvc.perform(get("/exchangeRates/getRange/GBP?from=2017-01-01&to=2018-02-01"))
				.andExpect(status().isOk());
	}

	@Test
	public void get_ExchangeRate() throws Exception {
		mockMvc.perform(get("/exchangeRates/getRate?date=2017-01-01&lcy=SGD&fcy=GBP"))
			//.andDo(print())
			.andExpect(status().isOk());
	}
	
	@Test
	public void get_ExchangeRate_NotFound() throws Exception {
		mockMvc.perform(get("/exchangeRates/getRate?date=2019-01-01&lcy=SGD&fcy=GBP"))
			//.andDo(print())
			.andExpect(status().isInternalServerError());
	}
}

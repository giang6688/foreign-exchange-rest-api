# foreign-exchange-rest-api

#### Build & run
```sh
mvn clean package
java -jar target\foreign-exchange-1.0-SNAPSHOT.jar
```
#### Test API
- Given a date, get the exchange rate of all the currencies (wrt USD)
```sh
curl http://localhost:8080/exchangeRates/getAll/2017-01-01
```
- Given a date and 2 currencies, find the exchange rate between them
```sh
http://localhost:8080/exchangeRates/getRate?date=2017-01-01&lcy=SGD&fcy=GBP
```
- Given a date range and a given currency, find the exchange rate of that currency (wrt USD) for the entire date range
```sh
curl http://localhost:8080/exchangeRates/getRange/GBP?from=2017-01-01&to=2018-02-12
```